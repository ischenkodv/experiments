def create_tour(nodes):
    tour = []
    l = len(nodes)
    for i in xrange(l):
        # Note the modulus division to connect last node with the first.
        t = ( nodes[i], nodes[(i + 1) % l])
        tour.append(t)

    return tour

print create_tour([1,2,3,4,5,6,7,8,9])
