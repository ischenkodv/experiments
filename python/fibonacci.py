# Mine version
def fibonacci_mine(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    
    n_minus_2 = 0
    n_minus_1 = 1
    
    i = 1
    f = 0
    while i < n:
        f = n_minus_2 + n_minus_1
        n_minus_2 = n_minus_1
        n_minus_1 = f
        i = i + 1

    return f

def fibonacci(n):
    current = 0
    after = 1
    for a in range(0,n):
        current,after = after, current + after
    return current

print fibonacci(36)
