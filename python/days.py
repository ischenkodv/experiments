daysOfMonths = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def isLeapYear(year):
    ##
    # Your code here. Return True or False
    # Pseudo code for this algorithm is found at
    # http://en.wikipedia.org/wiki/Leap_year#Algorithm
    ##
    if year % 400 == 0:
        return True
    else:
        if year % 100 == 0:
            return False
        else:
            return year % 4 == 0

def daysBetweenDates(year1, month1, day1, year2, month2, day2):
    ##
    # Your code here.
    ##
    year_begin = year1
    year_end = year2
    
    result = 0
    if year1 == year2:
        if month1 == month2:
            return day2 - day1
    
    
    while year1 <= year2:
        if year1 == year_begin:
            month_begin = month1 - 1
        else:
            month_begin = 0
            
        if year1 == year_end:
            month_end = month2 - 1
        else:
            month_end = 11
            
        while month_begin <= month_end:
            mDays = daysOfMonths[month_begin]
            if month_begin == 1 and isLeapYear(year1):
                mDays = mDays + 1

            if year1 == year_begin and month_begin == month1-1:
                days = mDays - day1
            else:
                if year1 == year_end and month_begin == month2-1:
                    days = day2
                else:
                    days = mDays

            result = result + days
            month_begin = month_begin + 1
            
        year1 = year1 + 1
            
    return result
            
        
    

# Test routine

def test():
    test_cases = [((2012,1,1,2012,2,28), 58), 
                  ((2012,1,1,2012,3,1), 60),
                  ((2011,6,30,2012,6,30), 366),
                  ((2011,1,1,2012,8,8), 585 ),
                  ((1900,1,1,1999,12,31), 36523)]
    for (args, answer) in test_cases:
        result = daysBetweenDates(*args)
        if result != answer:
            print "Test with data:", args, "failed"
        else:
            print "Test case passed!"

test()
