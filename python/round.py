x = 3.14159

# My solution
num_after_decimal = str(x*10)[str(x*10).find('.') - 1]
matrix = '098765'
shift = matrix.find(num_after_decimal)
x_converted = str((x * 10 + shift) / 10)

print x_converted[:x_converted.find('.')]

# Better solution
x_converted = str(x + 0.5)

print x_converted[:x_converted.find('.')]
