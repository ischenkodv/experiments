def russian(a, b):
    x = a
    y = b
    z = 0
    while x > 0:
        if x % 2 == 1: z = z + y
        y = y << 1
        x = x >> 1
    return z

print russian(20, 7)

import math

def time(n):
    """ Return the number of steps 
    necessary to calculate
    `print countdown(n)`"""
    steps = 0
    # YOUR CODE HERE
    loops = int( math.ceil(n / 5.0) )
    print 'loops: ' + str(loops)
    steps = 2 * loops + 3
    print 'steps: ' + str(steps)
    return steps

def countdown(x):
    y = 0
    while x > 0:
        x = x - 5
        y = y + 1
    print y

#print countdown(50)
print time(50)
print time(6)
