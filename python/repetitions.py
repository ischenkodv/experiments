# Question 8: Longest Repetition

# Define a procedure, longest_repetition, that takes as input a 
# list, and returns the element in the list that has the most 
# consecutive repetitions. If there are multiple elements that 
# have the same number of longest repetitions, the result should 
# be the one that appears first. If the input list is empty, 
# it should return None.

def longest_repetition(lst):
    prev = None
    
    max_repetitions = 0
    curr_repetitions = 0
    result = None
    
    for num in lst:
        if num != prev:
            curr_repetitions = 1
        else:
            curr_repetitions = curr_repetitions + 1

        if curr_repetitions > max_repetitions:
            max_repetitions = curr_repetitions
            result = num

        prev = num
        
    return result


#For example,

print longest_repetition([1, 2, 2, 3, 3, 3, 2, 2, 1])
# 3

print longest_repetition(['a', 'b', 'b', 'b', 'c', 'd', 'd', 'd'])
# b

print longest_repetition([1,2,3,4,5])
# 1

print longest_repetition([])
# None


print longest_repetition([2, 2, 3, 3, 3])
