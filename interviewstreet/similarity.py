
def calculate_similarity5(value):
    length = len(value)

    suffixes = []
    total = length

    suffixes = [value[i:] for i in range(1, length) if value[i] == value[0]]

    total += len(suffixes)
    for suffix in suffixes:
        for i in range(1, len(suffix)):
            if suffix[i] == value[i]:
                total += 1
            else:
                break

    return total

def calculate_similarity4(value):
    length = len(value)
    total = length

    for offset in range(1, length):
        j = 0
        if value[j] != value[offset]:
            continue
        else:
            j += 1

        for j in range(length - offset):
            if value[j] == value[j + offset]:
                j += 1
            else:
                break

        total += j

    return total

def calculate_similarity3(value):
    length = len(value)
    total = length

    for i in range(1, length):
        j = 0
        for w in value[i:]:
            if value[j] == w:
                j += 1
            else:
                break
        total += j

    return total

def calculate_similarity2(value):
    total = 0
    length = len(value)

    i = 0
    while i < length:
        j = 0
        for w in value[i:]:
            if value[j] == w:
                total += 1
                j += 1
            else:
                break
        i += 1

    return total

def calculate_similarity(value):
    total = 0
    for n in xrange(len(value)):
        suffix = value[n:]
        similarity = 0
        for i in xrange(len(suffix)):
            if suffix[i] == value[i]:
                similarity = similarity + 1
            else:
                break
                
        total = total + similarity
    return total

def calculate_similarity6(value):
    length = len(value)
    total = length

    for i in xrange(1, length):
        prefix = value[:i]
        suffix = value[-i:]

        if suffix[0] == prefix[0]:
            if suffix == prefix:
                total += i
                continue

            j = 1
            for w in suffix[1:-1]:
                if value[j] == w:
                    j += 1
                else:
                    break
            total += j

    return total

T = int(raw_input())
for i in xrange(T):
    print calculate_similarity6(raw_input())

