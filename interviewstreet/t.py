import similarity
import timeit

#print timeit.timeit('import similarity; similarity.calculate_similarity("abbaaabbaab")', number=300000)
#print timeit.timeit('import similarity; similarity.calculate_similarity2("abbaaabbaab")', number=300000)
#print timeit.timeit('import similarity; similarity.calculate_similarity3("abbaaabbaab")', number=300000)
print timeit.timeit('import similarity; similarity.calculate_similarity4("abbaaabbaab")', number=300000)
print similarity.calculate_similarity4("abbaaabbaab")
print '---------------------------'
print timeit.timeit('import similarity; similarity.calculate_similarity5("abbaaabbaab")', number=300000)
print similarity.calculate_similarity5("abbaaabbaab")
print '---------------------------'
print timeit.timeit('import similarity; similarity.calculate_similarity6("abbaaabbaab")', number=300000)
print similarity.calculate_similarity6("abbaaabbaab")
print '---------------------------'
print timeit.timeit('import similarity; similarity.calculate_similarity("aaaaaaaaaaaa")', number=300000)
print similarity.calculate_similarity4("aaaaaaaaaaaa")
print '---------------------------'
print timeit.timeit('import similarity; similarity.calculate_similarity5("aaaaaaaaaaaa")', number=300000)
print similarity.calculate_similarity5("aaaaaaaaaaaa")
print '---------------------------'
print timeit.timeit('import similarity; similarity.calculate_similarity6("aaaaaaaaaaaa")', number=300000)
print similarity.calculate_similarity6("aaaaaaaaaaaa")
print '---------------------------'
